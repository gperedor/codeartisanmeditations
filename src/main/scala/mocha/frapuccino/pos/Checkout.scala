package mocha.frapuccino.pos

import scala.math.BigDecimal

/** Checkout is a proposed solution to http://codekata.com/kata/kata01-supermarket-pricing/
  *
  * It's comprised of a Checkout class that scans items and calculates the total prices
  * according to a map of SaleRule
  * @author gabrielperedo
  */

/** The basic interface for a pricing rule
  */
trait SaleRule {
  /** Given the amount, calculate how much that number of items is worth
    * @param amount
    * @return The subtotal
    */
  def subtotal(amount: Int): BigDecimal
}

/** Yields the total price for items which are bought separately
  * @param unitPrice
  */
case class SinglePurchase (unitPrice: BigDecimal) extends SaleRule {
  override def subtotal(amount: Int): BigDecimal = unitPrice * BigDecimal(amount)
}

/** Yields the total price for items which are subject to promotions like 1 for $5, get 5 for $20
  * @param unitPrice The price if the number of items outside the promotion
  * @param xForY A tuple where x is the number of items and y the price for the bundle
  */
case class Promotion (unitPrice: BigDecimal, xForY: (Int, BigDecimal)) extends SaleRule {
  override def subtotal(amount: Int): BigDecimal = {
    val promo = amount / xForY._1
    val separate = amount % xForY._1
    (separate * unitPrice) + (promo * xForY._2)
  }
}

object SaleRule {
  /** Parses a sale rule
    *
    * Syntax is either:
    *
    * {Name} {Price}
    * {Name} {n} for {Price}
    *
    * @param line
    * @return A rule for the item
    */
  def read(line: String): Either[String, (String, SaleRule)] = {
    val parts = line.trim().split("\\s+", 3)
    val name = parts(0)
    try {
      val unitPrice = parts(1).toDouble
      if (parts.length > 2) {
        val xy = parts(2).split(" for ")
        Right(name, Promotion(unitPrice, (xy(0).toInt, xy(1).toDouble)))
      } else {
        Right(name, SinglePurchase(unitPrice))
      }
    } catch {
      case re: RuntimeException => {
        re.printStackTrace()
        Left("Invalid format")
      }
    }
  }
}

/** Processes items as if it were a cashier's checkout
  *
  * @param rules
  * @param amounts
  */
case class Checkout(rules: Map[String, SaleRule], amounts: Map[String, Int]) {
  /** Registers an item as part of a customer's purchase.
    *
    * Must be in the rules map or else it will be None
    * @param item
    * @return An updated checkout, or None if the item doesn't have an associates rule
    */
  def scan(item: String): Option[Checkout] = {
      if (rules.contains(item)) {
        Some(Checkout(rules, amounts.updated(item, amounts.getOrElse(item, 0) + 1)))
      } else {
        None
      }
  }

  /** Calculates total from rules
    *
    * @return The total price of the purchase
    */
  def total(): BigDecimal = {
    // If the Checkout registered the amounts through scan, there will only be valid items
    val subtotals = for ((k, v) <- amounts) yield rules.get(k).map(r => r.subtotal(v))
    val t = subtotals.reduce((total, subtotal) => for (t <- total; s <- subtotal) yield t + s)
    t.get
  }
}

object Checkout {
  def apply(rules: Map[String, SaleRule]) = new Checkout(rules, Map())
}