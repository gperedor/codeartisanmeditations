package mocha.frapuccino.pos

import mocha.frapuccino.pos
import mocha.frapuccino.pos._
import org.scalatest._
/**
  * @author gabrielperedo
  */
class CheckoutSpec extends FlatSpec {

  "A promotional rule" should "sum individual prices if the number of items is not met" in {
    val ruleA = Promotion(10.5, (4, 33))
    assert(ruleA.subtotal(3) === 31.5)
  }

  it should "yield a promotional value when the minimum number is met" in {
    val ruleA = Promotion(15, (5, 60))
    assert(ruleA.subtotal(5) === 60)
  }

  it should "yield the maximum number of promotional values plus spare items" in {
    val ruleA = Promotion(10, (4, 33))
    assert(ruleA.subtotal(13) === 109)
  }

  "Single purchase rules" should "yield only proportional amounts of money" in {
    val ruleB = SinglePurchase(10.5)
    assert(ruleB.subtotal(1) === 10.5)
    assert(ruleB.subtotal(10) === 105)
  }

  "SaleRule" should "read SinglePurchase rules" in {
    val sr = SaleRule.read("A 10.5")
    assert(sr.isRight)
    assertResult(Right(true)) {
      sr.right.map(r => r._2.isInstanceOf[SinglePurchase])
    }
  }

  it should "read Promotion rules" in {
    val sr = SaleRule.read("B 5 10 for 40")
    assert(sr.isRight)
    assertResult(Right(true)) {
      sr.right.map(r => r._2.isInstanceOf[Promotion])
    }
  }

  "Checkout" should "add items to the tally on scan" in {
    val ruleA = SinglePurchase(10)
    val ruleB = Promotion(11.5, (3, 33))
    val checkout = Checkout(Map("A" -> ruleA, "B" -> ruleB), Map())
    val newCheckout = checkout.scan("A").flatMap(_.scan("A")).flatMap(_.scan("B"))
    assertResult(Some(2)) {
      newCheckout.map(_.amounts.getOrElse("A", 0))
    }
    assertResult(Some(1)) {
      newCheckout.map(_.amounts.getOrElse("B", 0))
    }
  }

  it should "yield the total according to rules" in {
    val ruleA = SinglePurchase(10)
    val ruleB = Promotion(5, (5, 20))
    val withA = Checkout(Map("A" -> ruleA, "B" -> ruleB)).scan("A").flatMap(_.scan("A"))
    val withB = (1 to 6).foldRight(withA)((_, t) => t.flatMap(_.scan("B")))

    // 2*A + Promo(5,B) + B
    assert(Some(20 + 20 + 5) === withB.map(_.total()))
  }
  it should "fail to scan unrecongnized items" in {
    val ruleA = SinglePurchase(10)
    val checkout = Checkout(Map("A" -> ruleA))
    val withoutUnrecognized = checkout.scan("B")
    assert(withoutUnrecognized === None)
  }
}
